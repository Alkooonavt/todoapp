﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Todo.Data;
using Todo.Models;
using Task = Todo.Models.Task;

namespace Todo.Controllers
{
    [Authorize]
    public class TaskController : Controller
    {
        private readonly ApplicationDbContext _context;
        private IdentityUser _user;

        public TaskController(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index(string searchText)
        {

            var list = _context.Users;
            _user = _context.Users.FirstOrDefault(x => x.Email == HttpContext.User.Identity.Name);
            if (!string.IsNullOrEmpty(searchText))
            {
                var todoList = await _context.TodoList.Where(x => x.Name.Contains(searchText))
                    .Where(x => x.IdentityUser == _user).ToListAsync();
                return View(todoList);
            }
            else
            {
                var todoList = await _context.TodoList
                    .Where(x => x.IdentityUser == _user).ToListAsync();
                return View(todoList);
            }
        }

        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Task task)
        {
            _user = _context.Users.FirstOrDefault(x => x.Email == HttpContext.User.Identity.Name);
            if (!ModelState.IsValid) return View(task);
            task.IdentityUser = _user;
            _context.Add(task);
            await _context.SaveChangesAsync();
            var todoList = _context.TodoList.Where(x => x.IdentityUser == _user).ToList();
            return RedirectToAction("Index");
        }


        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null) return NotFound();
            var task = await _context.TodoList.FindAsync(id);
            if (task == null) return NotFound();
            return View(task);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, Task task)
        {
            if (task.Id != id) return NotFound();
            _user = _context.Users.FirstOrDefault(x => x.Email == HttpContext.User.Identity.Name);
            if (!ModelState.IsValid) return View(task);
            try
            {
                task.IdentityUser = _user;
                _context.Update(task);
                await _context.SaveChangesAsync();
            }
            catch
            {
                return NotFound();
            }
            return RedirectToAction("Index");

        }
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null) return NotFound();
            var task = await _context.TodoList.FindAsync(id);
            if (task == null) return NotFound();
            return View(task);
        }


        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id)
        {
            _user = _context.Users.FirstOrDefault(x => x.Email == HttpContext.User.Identity.Name);
            var task = await _context.TodoList.Where(x => x.IdentityUser == _user && x.Id == id).FirstOrDefaultAsync();
            if (task == null) return NotFound();

            _context.TodoList.Remove(task);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }


    }
}